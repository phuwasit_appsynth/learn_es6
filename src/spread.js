const arr1 = ['two', 'three']
const arr2 = ['one', ...arr1, 'four', 'five']
const arr3 = [...arr2]

console.log(arr2) // [ 'one', 'two', 'three', 'four', 'five' ]
console.log(arr3) // [ 'one', 'two', 'three', 'four', 'five' ]

const numbers = [50, 10, 70, 11]
const min = Math.min(...numbers)

console.log(min) // 10
