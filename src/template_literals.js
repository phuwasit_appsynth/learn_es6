const domain = 'localhost'
const port = 8080

const url = `http://${domain}:${port}/api`

console.log(url) // http://localhost:8080/api
