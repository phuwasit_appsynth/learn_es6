const todo = { text: 'learn es next', completed: false }

// const newState = Object.assign({}, todo, { completed: true })
const newState = { ...todo, completed: true }

console.log(todo)  // { text: 'learn es next', completed: false }
console.log(newState) // { text: 'learn es next', completed: true }
