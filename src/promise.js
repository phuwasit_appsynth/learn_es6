const add = (x, y) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x + y);
    }, 2000);
  })
}

let val = 1;
add(val, 10)
  .then(function(val) {
    return add(val, 10)
  })
  .then(function(val) {
    return add(val, 10)
  })
  .then(function(val) {
    console.log('Promise => ', val);
  })

console.log('End')