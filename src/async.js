const add = (x, y) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x + y);
    }, 2000);
  })
}

async function addAsync() {
  let val = 1
  val = await add(val, 10) // 11
  val = await add(val, 10) // 21
  val = await add(val, 10) // 31

  console.log('Async => ', val)
  console.log('End')
}

addAsync()

