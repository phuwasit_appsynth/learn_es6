const list = [1, 2, 3, 4]
const [a, b, ...rest] = list

console.log(a)    // 1
console.log(b)    // 2
console.log(rest) // [3, 4]

const route = { path: '/query', params: { name: 'react' } }

// const { path } = route
// console.log(path) // query

// const { path: url } = route
// console.log(path) // error: path is not defined
// console.log(url) // query

const { params: { name } } = route

// console.log(params) // error: path is not defined
console.log(name) // react
